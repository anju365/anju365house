define(['angular'], function (angular) {
    return angular.module('beequick', ['ui.router']);
});
