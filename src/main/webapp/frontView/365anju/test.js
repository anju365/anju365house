define(['app', 'jquery'], function(app, $) {
    // 'use strict';
    app.config(function($provide) {
        $provide.provider('MathService', function() {
            this.$get = function() {
                var factory = {};

                factory.multiply = function(a, b) {
                    return a * b;
                }
                return factory;
            };
        });
    });
    app.value("defaultInput", 5);
    app.service('CalcService', function(MathService) {
        this.square = function(a) {
            return MathService.multiply(a, a);
        }
    });
    app.controller('indexCtrl', ['$scope',
        '$timeout',
        '$http',
        "$filter",
        '$parse',
        'CalcService',
        'defaultInput',
        function($scope,
            $timeout,
            $http,
            $filter,
            $parse,
            CalcService,
            defaultInput) {

            $("head").append('<meta name="viewport" content="width=device-width, initial-scale=1" />');
            // $("head").append('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
            // $("head").append('<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />');
            $("head").append('<meta name="keywords" content="乌鲁木齐市存量房交易系统,乌鲁木齐房产交易,存量房交易系统" />');
            $("head").append('<meta name="description" content="乌鲁木齐市存量房交易系统,乌鲁木齐房产交易,存量房交易系统" />');
            $scope.number = defaultInput;
            $scope.result = CalcService.square($scope.number);

            $scope.square = function() {
                $scope.result = CalcService.square($scope.number);
            }

            $scope.firstName = "John";
            $scope.lastName = "Doe";
            $scope.nameData = [
                { name: 'Jani', country: 'Norway' },
                { name: 'Hege', country: 'Sweden' },
                { name: 'Kai', country: 'Denmark' }
            ];
            $scope.city = [
                { "id": "1", "name": "福建" },
                { "id": "2", "name": "广东" },
                { "id": "5", "name": "上海" },
                { "id": "4", "name": "北京" },
                { "id": "3", "name": "四川" }
            ]
            $scope.addressSelected = '上海';
            $scope.addressList = ['上海', '北京', '深圳', '郑州', '周口', '西华'];


            function badgeabc(data) {
                console.info(data)
            }
            $scope.getWeatherInfo = function(address) {
                $http.jsonp('http://sapi.k780.com/?app=weather.future&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json&jsoncallback=JSON_CALLBACK&weaid=' +
                    encodeURI(address)).success(function(data) {
                    $scope.weatherinfo = data.result;
                });
            }



            $scope.onAddress = function(address) {
                console.info(address)
            }
            console.info(app)

            $scope.onTimeout = function() {
                $scope.myHeader = "Hello World!";
                $timeout(function() {
                    $scope.myHeader = "How are you today?";
                }, 2000);
            }

            $scope.selected = '';
            $scope.optData = [{
                id: 10001,
                MainCategory: '男',
                ProductName: '水洗T恤',
                ProductColor: '白'
            }, {
                id: 10002,
                MainCategory: '女',
                ProductName: '圓領短袖',
                ProductColor: '黃'
            }, {
                id: 10003,
                MainCategory: '女',
                ProductName: '圓領短袖',
                ProductColor: '黃'
            }];

            var watch = $scope.$watch('addressSelected', function(newValue, oldValue, scope) {
                $scope.getWeatherInfo(newValue);
            });



            $scope.colors = [
                { name: 'black', shade: 'dark' },
                { name: 'white', shade: 'light' },
                { name: 'red', shade: 'dark' },
                { name: 'blue', shade: 'dark' },
                { name: 'yellow', shade: 'light' }
            ];
            $scope.object = {
                dark: "black",
                light: "red",
                lai: "red"
            };


            // if (document.all) document.write('<!--[if lte IE 6]><script type="text/javascript">window.ie6= true<\/script><![endif]-->');
            // $scope.change = function(picId, fileId) {
            //     //使用IE条件注释来判断是否IE6，通过判断userAgent不一定准确

            //     // var ie6 = /msie 6/i.test(navigator.userAgent);//不推荐，有些系统的ie6 userAgent会是IE7或者IE8
            //     // function change(picId, fileId) {
            //     var pic = document.getElementById(picId);
            //     var file = document.getElementById(fileId);
            //     if (window.FileReader) { //chrome,firefox7+,opera,IE10+
            //         var oFReader = new FileReader();
            //         oFReader.readAsDataURL(file.files[0]);
            //         oFReader.onload = function(oFREvent) { pic.src = oFREvent.target.result; };
            //     } else if (document.all) { //IE9-//IE使用滤镜，实际测试IE6设置src为物理路径发布网站通过http协议访问时还是没有办法加载图片
            //         file.select();
            //         file.blur(); //要添加这句，要不会报拒绝访问错误（IE9或者用ie9+默认ie8-都会报错，实际的IE8-不会报错）
            //         var reallocalpath = document.selection.createRange().text //IE下获取实际的本地文件路径
            //         //if (window.ie6) pic.src = reallocalpath; //IE6浏览器设置img的src为本地路径可以直接显示图片
            //         //else { //非IE6版本的IE由于安全问题直接设置img的src无法显示本地图片，但是可以通过滤镜来实现，IE10浏览器不支持滤镜，需要用FileReader来实现，所以注意判断FileReader先
            //         pic.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='image',src=\"" + reallocalpath + "\")";
            //         pic.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='; //设置img的src为base64编码的透明图片，要不会显示红xx
            //         // }
            //     } else if (file.files) { //firefox6-
            //         if (file.files.item(0)) {
            //             url = file.files.item(0).getAsDataURL();
            //             pic.src = url;
            //         }
            //     }


            //     var fileIn = file.files[0];

            //     // var img = new Image();
            //     var fileType = fileIn.name.substring(fileIn.name.lastIndexOf(".") + 1, fileIn.name.length);
            //     if (fileIn.size > 1048576 * 1) { //单位是B，此处不允许超过5M
            //         alert("图片不能超过5M")
            //         return;
            //     }
            //     if (fileType == 'JPG' || fileType == 'PNG' || fileType == 'JPEG ' || fileType == 'jpg' || fileType == 'png' || fileType == 'jpeg') {} else {
            //         alert("图片格式只支持:JPG,PNG,JPEG")
            //         return;
            //     }
            //     // fileReader.readAsDataUrl(fileIn, $scope)
            //     //     .then(function(result) {
            //     //         $scope.imageSrc = result;
            //     //         console.log(img.width);
            //     //     });
            // }


            $scope.onFileSelect = function(files) {
                // alert(11)
                // var fileIn = files[0];

                // console.info(fileIn)
                // var img = new Image();
                // var fileType = fileIn.name.substring(fileIn.name.lastIndexOf(".") + 1, fileIn.name.length);
                // if (fileIn.size > 1048576 * 1) { //单位是B，此处不允许超过5M
                //     alert("图片不能超过5M")
                //     return;
                // }
                // if (fileType == 'JPG' || fileType == 'PNG' || fileType == 'JPEG ' || fileType == 'jpg' || fileType == 'png' || fileType == 'jpeg') {} else {
                //     alert("图片格式只支持:JPG,PNG,JPEG")
                //     return;
                // }
                // fileReader.readAsDataUrl(fileIn, $scope)
                //     .then(function(result) {
                //         $scope.imageSrc = result;
                //         console.log(img.width);
                //     });
            }



            $scope.datalist = [
                { name: "ccc", age: 10 },
                { name: "aaa", age: 50 },
                { name: "eeee", age: 30 },
                { name: "addd", age: 20 },
                { name: "bbb", age: 40 },
            ]


            $scope.handleChange = function() {
                //$scope.userinput 输入框中的内容 ，datalist为 数组中的数据
                //filter也是关键字。相当于orderBy
                // alert(11)
                // $scope.datalist = $filter("filter")($scope.datalist, $scope.userinput);
                console.log($scope.datalist);
                // console.log(datalist);
            }

        }
    ]);
    //自定义过滤器
    app.filter('reverse', function() { //可以注入依赖
        return function(text) {
            return text.split("").reverse().join("");
        }
    });



    app.filter("limitCharacter", function($filter) {
        //reuse AngularJS filter limitTo and append "..." in case of long text, excluding HTML tags
        return function(text, limit) {
            var plainText = text.replace(/<\/?([^>])+>/g, "");
            if (text && plainText.length > limit) {
                return $filter("limitTo")(plainText, limit) + "..."; //&hellip;";
            } else {
                return text;
            }
        }
    });
    app.factory("fileReader", function($q, $log) {
        var onLoad = function(reader, deferred, scope) {
            return function() {
                scope.$apply(function() {
                    deferred.resolve(reader.result);
                });
            };
        };
        var onError = function(reader, deferred, scope) {
            return function() {
                scope.$apply(function() {
                    deferred.reject(reader.result);
                });
            };
        };
        var onProgress = function(reader, scope) {
            return function(event) {
                scope.$broadcast("fileProgress", {
                    total: event.total,
                    loaded: event.loaded
                });
            };
        };
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
        var readAsDataURL = function(file, scope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };
        return {
            readAsDataUrl: readAsDataURL
        };
    });



});