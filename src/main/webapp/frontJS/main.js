// 'use strict';
(function(win) {
    //配置baseUrl
    // var baseUrl = document.getElementById('main').getAttribute('data-baseurl');
    /*
     * 文件依赖
     */
    var config = {
        baseUrl: './',
        // baseUrl: baseUrl,           //依赖相对路径
        paths: { //如果某个前缀的依赖不是按照baseUrl拼接这么简单，就需要在这里指出
            'jquery': '../../frontJS/jquery',
            angular: '../../frontJS/angular/angular-1.2.27',
            'angular-ui-router': '../../frontJS/angular/angular-ui-router',
            angularAMD: '../../frontJS/angular/angularAMD',
            domReady: '../../frontJS/angular/domReady',
            common:'../../frontJS/common.angular',
            html5:'../../frontJS/html5shiv',
            app: './app',
            indexCtrl: './test'
        },
        shim: {
            deps: ["jquery"],
            angular: {
                exports: 'angular'
            },
            'angular-ui-router': {
                deps: ['angular']
            },
            angularAMD: {
                deps: ['angular']
            }
        }
    }
    require.config(config);
    requirejs([
        'angular', 'domReady', 'app', 'angular-ui-router','indexCtrl','common'
    ], function(angular, domReady) {
        domReady(function() {
            angular.bootstrap(document.querySelector('#App'), ['beequick']);
        });
    });
})(window);