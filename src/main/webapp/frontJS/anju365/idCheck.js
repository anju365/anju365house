function toSee(_idNumber,_id){
	
	$(".passBtn2").hide();
	$('#dialogMakeTrue').find('#userTip').text('');
    $('#dialogMakeTrue').attr('data-idNumber',_idNumber).attr('data-id',_id).show();
	$('#dialog-cover').show();
	$('#dialogMakeTrue').show();
	$('#dialogMakeTrue').css({
	      'top':($(window).height()-$('#dialogMakeTrue').height())/2,
          'left':($(window).width()-$('#dialogMakeTrue').width())/2
  });
	
	$.ajax({
		url:'../../regist/findByIdNumber.svc',
		type:'POST',
		data:JSON.stringify({
			idNumber:$('#dialogMakeTrue').attr('data-idNumber')
		}),
		success:function(data){
			if(data.succ){
				if(data.result.userStatus==1){
					$(".passBtn2").hide();
				}else{
					$(".passBtn2").show();
				}
				var imgSrc=data.result.cardImg.split(",");
				$(".userName").val(data.result.userName);
				$(".idNumber").val(data.result.idNumber);
				$(".tImg").attr('src',imgSrc[0]);
				$(".fImg").attr('src',imgSrc[1]);
			}else{
				$(".userTip").text(data.message);
			}
		},
		error:function(data){
			$(".userTip").text('查询详情失败，请联系技术人员处理！');
		}
	})
}
//

function logout(){
	window.location.href = '../../login/logout.svc';
}
//获得选择框(复选框/单选框)选中单元的值
//参数 nameStr 求值元素的name属性
//参数 fatherObj 需要在哪个DOM元素下执行 如果没有传递则使用document
function openMakeSure(text){
	$('#makeTrue').find('.userTip').text(text);
 	$('#dialog-cover').show();
	$('#makeTrue').show();
	$('#makeTrue').css({
	      'top':($(window).height()-$('#makeTrue').height())/2,
          'left':($(window).width()-$('#makeTrue').width())/2
  });
}
function isArray(o){
	return Object.prototype.toString.call(o)=='[object Array]';
}

//批量处理
function allHandle(type){
	var _idData = [];
	$('table').find('input[type="checkbox"]:checked').each(function(k,v){
		var _obj = {};
		_obj.id = $(this).attr('data-id');
		_obj.userStatus =type;
		_idData.push(_obj);
	});
	
	if(_idData.length == 0){
		return false;
	}
	
	$.ajax({
		url:'../../regist/updateUserInfo.svc',
		type:'POST',
		data:JSON.stringify({
			userComEntity:_idData
		}),
		success:function(data){
			if(data.succ){
				openMakeSure('批量审核成功！')
				openMakeSure('用户审核成功！')
			}else{
				openMakeSure(data.message);
			}
		},
		error:function(){
			openMakeSure('批量审核失败，请联系技术人员处理！');
		}
	});
}

function isPass(){
	var idData=[];
	var obj={};
	obj.id=$('#dialogMakeTrue').attr('data-id');
	obj.userStatus=1;
	idData.push(obj);
	$.ajax({
		url:'../../regist/updateUserInfo.svc',
		type:'POST',
		data:JSON.stringify({
			userComEntity:idData
		}),
		success:function(data){
			if(data.succ){
				window.location.href='../../frontView/anju365/idCheck.html';
			}else{
				$(".userTip").text(data.message);
			}
		},
		error:function(){
			$(".userTip").text('审核失败，请联系技术人员处理！');
		}
	});
}
function unPass(){
	var idData=[];
	var obj={};
	obj.id=$('#dialogMakeTrue').attr('data-id');
	obj.userStatus=2;
	idData.push(obj);
	$.ajax({
		url:'../../regist/updateUserInfo.svc',
		type:'POST',
		data:JSON.stringify({
			userComEntity:idData
		}),
		success:function(data){
			if(data.succ){
				window.location.href='../../frontView/anju365/idCheck.html';
			}else{
				$(".userTip").text(data.message);
			}
		},
		error:function(){
			$(".userTip").text('审核失败，请联系技术人员处理！');
		}
	});
}


//关闭弹框
function closeRightManage(){
	$('.dialog').hide();
	$('#dialog-cover').hide();
	$('.dialog').css({
	    'top':0,
	    'left':0
	});
	window.location.reload();
}

//获得选择框(复选框/单选框)选中单元的值
//参数 nameStr 求值元素的name属性
//参数 fatherObj 需要在哪个DOM元素下执行 如果没有传递则使用document
function JS_getCheckValue(nameStr, fatherObj){
	var Checkers
	if (!fatherObj || typeof(fatherObj)!= "object"){
		Checkers = document.getElementsByTagName("input");
	}else{
		Checkers = fatherObj.getElementsByTagName("input");
	}
	if (!Checkers) return false;
	var valueStr="";
	for (var i=0;i<Checkers.length;i++){
		if (Checkers[i].name==nameStr && Checkers[i].checked==true){
			valueStr += "," + Checkers[i].value;
		}
	}
	if (valueStr){
		valueStr=valueStr.substr(1);
	}
	console.log(valueStr);
	return valueStr;
	
}
//html页面dom填充渲染
function ajaxDom(pageNum,pageSize,obj){
	var _html = '';
	var sta=['未审核','已通过','未通过'];
	for(var i=0;i<obj.length;i++){
		_html += '<tr>\
		<td><input onclick="clickInput()" data-id="'+obj[i].id+'" type="checkbox"/></td>\
		<td>'+(pageSize*(pageNum-1)+i+1)+'</td>\
		<td>'+(obj[i].userCode ? obj[i].userCode : "--")+'</td>\
		<td class="name">'+(obj[i].userName ? obj[i].userName : "--")+'</td>\
		<td>'+(obj[i].idNumber ? obj[i].idNumber : "--")+'</td>';
		if(obj[i].userStatus==0){
			_html+='<td style="color:#80b3d7">'+(sta[obj[i].userStatus] ? sta[obj[i].userStatus] : "--")+'</td>'+
			'<td><a class="detail-a" href="javascript:toSee(\'' + obj[i].idNumber+'\',\''+obj[i].id+  '\')">查看</a></td>'+
	        '</tr>';
		}else if(obj[i].userStatus==1){
			_html+='<td style="color:#12b067">'+(sta[obj[i].userStatus] ? sta[obj[i].userStatus] : "--")+'</td>'+
			'<td><a class="detail-a" href="javascript:toSee(\'' + obj[i].idNumber+ '\',\''+obj[i].id+ '\')">查看</a></td>'+
	        '</tr>';
		}else if(obj[i].userStatus==2){
			_html+='<td style="color:red">'+(sta[obj[i].userStatus] ? sta[obj[i].userStatus] : "--")+'</td>'+
			'<td><a class="detail-a" href="javascript:toSee(\'' + obj[i].idNumber+'\',\''+obj[i].id+ '\')">查看</a></td>'+
	        '</tr>';
		}																	
	 }
	$('table tbody').html(_html);
	$("tr:even").css("background","#f5f5f5");
}
//加载列表数据函数
function loadData(idNumber,useName,userStatus,pageNum,pageSize){
	/* obtain data */
	$.ajax({
		url:'../../regist/getUserInfoPage.svc',
		type:'POST',
		data:JSON.stringify({
			pageNum:pageNum,
			pageSize:pageSize,
			userComEntity:{
				idNumber:idNumber,
				userName:userName,
				userStatus:userStatus
				
			}
		}),
		success:function(data){
			if(data.succ){
				var _total = data.result.total;
				if(isArray(data.result.rows)){
					if(data.result.rows.length != 0){
						var _rows = data.result.rows;
						var _list = [];
						for(var i=0;i<_rows.length;i++){
							var _obj = {
								idNumber:_rows[i].idNumber,
								userName:_rows[i].userName,
								userCode:_rows[i].userCode,
								userStatus:_rows[i].userStatus
							}
							_list.push(_obj);
						}
						ajaxDom(pageNum,pageSize,_rows);
					}else{
						$('table tbody').html('<tr><td class="err-td" colspan="7">暂无数据</td></tr>');
					}
				}
				
				
				$('.totalCount').text(_total); 
                $('.currentPage').text(pageNum)
				$('.totalPage').text(Math.ceil(_total/pageSize));
				if(flag){
					$('#pageTool').html('').Paging({
			            pagesize:pageSize,
			            count:_total,
			            callback:function(page,size,count){
			            	pageNum = page;
			            	pageSize=10;
			                $.ajax({
			            		url:'../../regist/getUserInfoPage.svc',
			            		type:'POST',
			            		data:JSON.stringify({
			            			pageNum:pageNum,
			            			pageSize:pageSize,
			            			userComEntity:{
			            				idNumber:idNumber,
			            				userName:userName,
			            				userStatus:userStatus
			            			}
			            		}),
			            		success:function(data){
			            			if(data.succ){
		            					var _total = data.result.total;
		            					if(isArray(data.result.rows)){
		            						if(data.result.rows.length != 0){
		            							var _rows = data.result.rows;
		            							var _list = [];
		            							for(var i=0;i<_rows.length;i++){
		            								var _obj = {
		            									idNumber:_rows[i].idNumber,
		            									userName:_rows[i].userName,
		            									userCode:_rows[i].userCode,
		            									userStatus:_rows[i].userStatus
		            								}
		            								_list.push(_obj);
		            							}
		            							ajaxDom(pageNum,pageSize,_rows);
		            						}else{
		            							$('table tbody').html('<tr><td class="err-td" colspan="7">暂无数据</td></tr>');
		            						}
		            					}
			            			}else{
			            				$('table tbody').html('<tr><td class="err-td" colspan="7">'+data.message+'</td></tr>');
			            			}
			            		},
			            		error:function(){
			            			$('table tbody').html('<tr><td class="err-td" colspan="7">数据查询失败，请重新尝试，或联系技术人员处理！谢谢~</td></tr>');
			            		}
			                });
			            }
			        });
					flag = false;
				}
				
			}else{
				$('table tbody').html('<tr><td class="err-td" colspan="7">'+data.message+'</td></tr>');
			}
		},
		error:function(){
			$('table tbody').html('<tr><td class="err-td" colspan="7">数据查询失败，请重新尝试，或联系技术人员处理！谢谢~</td></tr>');
    	}
	});
}


function clickInput(){
	if($('table').find('input[type="checkbox"]:checked').length==0){
		$(".isPass").attr('disabled',true);
		$(".unPass").attr('disabled',true);
		$(".isPass").css("background",'#9cbdd5');
		$(".unPass").css("background",'#ff9786');
	}else{
		$(".isPass").attr('disabled',false);
		$(".unPass").attr('disabled',false);
		$(".isPass").css("background",'#3d7cab');
		$(".unPass").css("background",'#ff310c');
	}
	
}
var pageNum = 1;
var pageSize = 10;
var flag = true;
var userName='';
var idNumber='';
var userStatus='';

$(function(){
	$(".isPass").attr('disabled',true);
	$(".unPass").attr('disabled',true);
	//页面加载
	loadData(idNumber,userName,userStatus,pageNum,pageSize);
	
	$('#searchBtn').on('click',function(){
		pageNum = 1;
		pageSize = 10;
		flag = true,
		idNumber = $.trim($('#idNumber').val());
		userName = $.trim($('#userName').val());
		userStatus= $(".searchForStatus").val();
		loadData(idNumber,userName,userStatus,pageNum,pageSize);
	});
	
});