/*angular模块部分*/
var app=angular.module('findPassApp',[]);
app.controller("findPassCtrl",function($scope,$http){
	$scope.state={
		saveBtnTrue:false,
		idTrue:false,
		checkTrue:false,
		step1:true,
		step2:false,
		step3:false,
		step4:false,
		passTrue:false,
		repassTrue:false
		
		
	}
	
	$scope.dataModel={
		idNumber:'',
		idErrtip:'请输入18位身份证号码!',
		checkTip:'',
		step1Err:'',
		step2Err:'',
		step3Err:'',
		password:'',
		rePassword:'',
		passwordTip:'字母开头的6-18位数字、字母或下划线！',
		rePasswordTip:'请重新输入密码！'
	}
	
	$scope.ui={
			updateErr:function(){
				$scope.dataModel.idErrtip='请输入18位身份证号码!';
				$scope.dataModel.step1Err='';
				$scope.dataModel.step2Err='';
				$scope.dataModel.step3Err='';
				$scope.dataModel.passwordTip='字母开头的6-18位数字、字母或下划线！';
				$scope.dataModel.rePasswordTip='';
			},
			checkIdnumber:function(){
				$scope.dataModel.step1Err='';
				if(/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test($scope.dataModel.idNumber)){
					$scope.state.idTrue=true;
					$scope.dataModel.idErrtip='';
				}else{
					$scope.state.idTrue=false;
					$scope.dataModel.idErrtip='请输入18位身份证号码!';
				}
			},
			verifyId:function(){
				if(/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test($.trim($scope.dataModel.idNumber))){
					$scope.state.idTrue=true;
					$scope.dataModel.idErrtip='';
					$.ajax({
						 url:'../../foundPassword/foundPass.svc',
						 type:"POST",
						 data:JSON.stringify({
							idNumber:$scope.dataModel.idNumber
			    		}),
			    		success:function(data){
			    			$scope.$apply(function(){
			    				if(data.succ){
			    					console.log(data);
									if(data.result){
										$scope.state.idTrue=true;
										$scope.state.step1=false;
										$scope.state.step2=true;
										$scope.state.step3=false;
										$scope.state.step4=false;
										$scope.dataModel.userName=data.result.userName;
										$scope.dataModel.idNumber=data.result.idNumber;	
											
									}else{
										$scope.state.idTrue=true;
										$scope.dataModel.step1Err='该用户不存在！';
									}
									
				    			}else{
									$scope.state.idTrue=true;
									$scope.dataModel.step1Err=data.message;
				    			}
			    			})
			    		},
			    		error:function(data){
							$scope.state.idTrue=false;
							$scope.dataModel.step1Err='身份证号验证失败，请联系技术人员处理！';
			    		}
					})
				}else{
					$scope.state.idTrue=false;
					$scope.dataModel.step1Err='身份证号格式不对，请重新输入！'
				}
					
				
			},
			setNew:function(){
				$scope.state.step1=false;
				$scope.state.step2=false;
				$scope.state.step3=true;
				$scope.state.step4=false;
			},
			keyUpVerifyFirst:function(){
				
				if(/^[a-zA-Z]\w{5,17}$/.test($scope.dataModel.password)){
					$scope.state.passTrue=true;
					$scope.dataModel.setNewPassErr='';
				}else{
					$scope.state.passTrue=false;
					$scope.dataModel.passTip='字母开头的6-18位数字、字母或下划线！';
				}
				if($scope.state.passTrue&&$scope.state.repassTrue){
					$scope.state.saveBtnTrue=true;
					$("#saveBtn").css('background','#ff4d0e');
					$scope.state.repassTrue=true;
					$scope.state.passTrue=true;
				}else{
					$scope.state.saveBtnTrue=false;
					$scope.state.repassTrue=false;
					$scope.state.repassTrue=false;
				}
				
			},
			keyUpVerify:function(){
				
				if($scope.dataModel.rePassword==$scope.dataModel.password){
					$scope.state.repassTrue=true;
				}else{
					$scope.state.repassTrue=false;
					$scope.dataModel.rePasswordTip='密码输入不一致，请重新输入！'
				}
				
				if($scope.state.passTrue&&$scope.state.repassTrue){
					$scope.state.saveBtnTrue=true;
					$("#saveBtn").css('background','#ff4d0e');
					$scope.state.repassTrue=true;
					$scope.state.passTrue=true;
				}else{
					$scope.state.repassTrue=false;
					$scope.state.repassTrue=false;
				}
			},
			saveNew:function(){
				if($scope.state.passTrue&&$scope.state.repassTrue){
					$scope.state.saveBtnTrue=true;
					$("#saveBtn").css('background','#ff4d0e');
					$scope.state.repassTrue=true;
					$scope.state.passTrue=true;
					$.ajax({
						 url:'../../foundPassword/updatePassword.svc',
						 type:"POST",
						 data:JSON.stringify({
							userComEntity:{
								 idNumber:$scope.dataModel.idNumber,
								 password:$scope.dataModel.password
							}
			    		}),
			    		success:function(data){
			    			$scope.$apply(function(){
			    				if(data.succ){
				    				$scope.state.step1=false;
				    				$scope.state.step2=false;
				    				$scope.state.step3=false;
				    				$scope.state.step4=true;
				    			}else{
				    				$scope.dataModel.step3Err=data.message;
				    			}
			    			})
			    		 },
			    		    
			    	    error:function(data){
			    	    	$scope.dataModel.step3Err='新密码设置失败请联系技术人员处理！';
			    	    }
			    	})
				}else{
					$scope.dataModel.password='';
					$scope.dataModel.rePassword='';
					$scope.state.repassTrue=false;
					$scope.state.repassTrue=false;
				}
				
			}
	}
	
	
});

