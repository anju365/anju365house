package com.sys.usercom.entity;

import java.util.Date;

import com.eshop.frame.base.entity.BaseEntity;

public class UserComEntity extends BaseEntity {
	/**
	 * 普通用户实体
	 */
	private static final long serialVersionUID = 1L;
	private String id; // 用户id
	private String userCode; // 用户登录名
	private String password; // 登录密码
	private String userName; // 用户姓名
	private String idNumber; // 身份证号
	private String cardImg; // 身份证照片
	private String userTel; // 联系方式
	private Integer userStatus; // 用户状态 用户状态1有效0无效
	private Integer isLock; // 是否锁 0无锁 1有锁
	private Integer userCatg; // 用户种类 （0 系统用户 1普通用户）
	private Date createDate; // 创建时间
	private Date updateDate; // 修改 时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getCardImg() {
		return cardImg;
	}

	public void setCardImg(String cardImg) {
		this.cardImg = cardImg;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getIsLock() {
		return isLock;
	}

	public void setIsLock(Integer isLock) {
		this.isLock = isLock;
	}

	public Integer getUserCatg() {
		return userCatg;
	}

	public void setUserCatg(Integer userCatg) {
		this.userCatg = userCatg;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
