package com.sys.usercom.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.eshop.frame.SystemConfig;
import com.eshop.frame.annotation.Request;
import com.eshop.frame.exception.BusinessException;
import com.eshop.frame.exception.SystemException;
import com.eshop.frame.lang.CodeGenerators;
import com.github.pagehelper.StringUtil;
import com.google.common.collect.Lists;
import com.sys.usercom.dao.UserComMapper;
import com.sys.usercom.entity.UserComEntity;

/**
 * 
 * 用户注册
 *
 */
@Service
public class RegistService {
	@Autowired
	private UserComServiceImpl comServiceImpl;
	@Autowired
	private UserComMapper comMapper;
	private static final Logger logger = LoggerFactory.getLogger(RegistService.class);

	/**
	 * 用户照片的上传
	 */
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false, rollbackFor = Exception.class)
	public String photoSave(@Request("file") MultipartFile file, HttpServletRequest request) {
		if (file.isEmpty()) {
			throw new BusinessException("PHOTO_FILE_IS_EMPTY");
		}
		String path = request.getSession().getServletContext().getRealPath("") + File.separator + "tempPhoto";
		String fileName = file.getOriginalFilename();
		logger.info("图片上传参数：" + file.getName());
		if (StringUtil.isEmpty(fileName)) {
			throw new BusinessException("PARAMS_IS_NULL").addScene("obj", "图片信息上传-");
		}
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String newFileName = CodeGenerators.nextId() + "" + fileName.substring(fileName.lastIndexOf("."));
		FileOutputStream imgOut = null;// 根据 dir 抽象路径名和 img 路径名字符串创建一个新 File
		try {
			imgOut = new FileOutputStream(new File(dir, newFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			imgOut.write(file.getBytes());// 返回一个字节数组文件的内容
			imgOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String url = SystemConfig.getInstance().getString("photo.save.url");
		return url + newFileName;
	}

	/**
	 * 效验用户名是否重复
	 */
	public Long userIsPro(@Request("userCode") String userCode) {
		if (null == userCode) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		return comMapper.userIsPro(userCode);
	}

	/**
	 * 效验身分证号是否重复
	 */
	public Long idNumberIsPro(@Request("idNumber") String idNumber) {
		if (null == idNumber) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		return comMapper.idNumberIsPro(idNumber);
	}

	/**
	 * 效验手机号 是否重复
	 */
	public Long userTelIsPro(@Request("userTel") String userTel) {
		if (null == userTel) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		return comMapper.userTelIsPro(userTel);
	}

	/**
	 * 用户信息的注册
	 * 
	 */
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false, rollbackFor = Exception.class)
	public void addUserComEntity(@Request("userComEntity") UserComEntity userComEntity, HttpServletRequest request) {
		if (null == userComEntity) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		try {
			comServiceImpl.addUserInfo(userComEntity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用户的列表查询
	 */
	public List<UserComEntity> getUserInfoPage(@Request("userComEntity") UserComEntity userComEntity,
			@Request("pageNum") Integer pageNum, @Request("pageSize") Integer pageSize) {
		if (null == userComEntity) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		List<UserComEntity> list = comMapper.getUserInfo(userComEntity);
		return list;
	}

	/**
	 * 用户状态批量更新
	 * 
	 */
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false, rollbackFor = Exception.class)
	public void updateUserInfo(@Request("userComEntity") UserComEntity[] userComEntity, @Request("type") String type,
			HttpServletRequest request) {
		if (null == userComEntity || userComEntity.length == 0) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		try {
			comMapper.updateUserComEntity(Lists.newArrayList(userComEntity));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据身份证查询用户信息
	 * 
	 */
	public UserComEntity findByIdNumber(@Request("idNumber") String idNumber) {
		if (null == idNumber) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		UserComEntity userComEntity = comMapper.findByIdNumber(idNumber);
		return userComEntity;
	}
}
