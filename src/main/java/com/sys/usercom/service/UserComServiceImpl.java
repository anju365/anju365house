package com.sys.usercom.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eshop.frame.SystemConfig;
import com.eshop.frame.exception.BusinessException;
import com.eshop.frame.lang.CodeGenerators;
import com.eshop.frame.lang.Excels;
import com.eshop.frame.web.SessionUtils;
import com.sys.usercom.dao.UserComMapper;
import com.sys.usercom.entity.UserComEntity;
import com.sys.utils.MD5Util;

@Service("userComService")
public class UserComServiceImpl implements UserComService {
	@Autowired
	private UserComMapper userComMapper;

	public UserComEntity getUserByLoginName(String loginName, char[] password) {
		return this.checkUser(loginName, String.valueOf(password));
	}

	@Override
	public UserComEntity getUserInfo(HttpServletRequest req) {
		// Auto-generated method stub
		return SessionUtils.getUser(req);
	}

	public UserComEntity checkUser(String loginName, String password) {
		UserComEntity entity = userComMapper.checkUserCom(loginName);
		if (null == entity) {
			throw new BusinessException("USERCODE_OR_PASSWORD_IS_WRONG");
		}
		if (entity.getUserStatus() == 0) {
			throw new BusinessException("USERCODE_IS_NOT_UNAUDITING");
		}
		if (entity.getUserStatus() == 2) {
			throw new BusinessException("USERCODE_IS_NOT_PASS");
		}
		if (!MD5Util.MD5(password).equals(entity.getPassword())) {
			throw new BusinessException("USERCODE_OR_PASSWORD_IS_WRONG");
		}
		return entity;
	}

	/**
	 * 添加用户信息
	 *
	 * @param
	 */
	public void addUserInfo(@Param("userComEntity") UserComEntity userComEntity) {
		if (userComMapper.userIsPro(userComEntity.getUserCode()) > 0) {
			throw new BusinessException("USER_CODE_IS_EXIST ");
		}
		if (userComMapper.idNumberIsPro(userComEntity.getIdNumber()) > 0) {
			throw new BusinessException("IDNUMBER_IS_EXIST ");
		}
		if (userComMapper.userTelIsPro(userComEntity.getUserTel()) > 0) {
			throw new BusinessException("USER_TEL_IS_EXIST ");
		}
		// 补全用户信息
		userComEntity.setId(CodeGenerators.nextId() + "");
		// 用户密码MD5加密
		userComEntity.setPassword(MD5Util.MD5(userComEntity.getPassword()));
		userComMapper.addUserComEntity(userComEntity);

	}

	/**
	 * 用户信息更新
	 *
	 * @param userInfo
	 *            用户信息
	 * @param type
	 *            类型 0 批量禁用更新 1 批量启用更新 2 单条信息更新
	 * @param request
	 */

	/*
	 * public void updateUserInfo(@Request("userInfo") UserEntity[]
	 * userInfo, @Request("type") String type, HttpServletRequest request) { //
	 * 获取登录用户信息 UserEntity user = SessionUtils.getUser(request); for (UserEntity
	 * userentity : userInfo) { userentity.setCreateName(user.getUserCode()); //
	 * type 0 用户批量禁用 if ("0".equals(type)) { userentity.setUserStatus(0); } //
	 * type 1 用户批量启用 if ("1".equals(type)) { userentity.setUserStatus(1); }
	 * 
	 * } loginMapper.updateUserInfo(Lists.newArrayList(userInfo));
	 * 
	 * }
	 */

	/**
	 * 用户信息列表查询
	 *
	 * @param userInfo
	 * @return
	 */
	/*
	 * public List<UserEntity> getUserInfoPage(@Request("userInfo") UserEntity
	 * userInfo,
	 * 
	 * @Request("pageNum") Integer pageNum, @Request("pageSize") Integer
	 * pageSize) { List<UserEntity> list = loginMapper.getUserInfo(userInfo);
	 * return list;
	 * 
	 * }
	 */

	/**
	 * 登录密码MD5加密
	 *
	 * @param initPassword
	 * @return
	 */
	public static String getMD5LoginPassword(String initPassword) {
		String password = MD5Util.MD5(initPassword);
		return password;
	}

	/**
	 * 上传文件
	 *
	 * @param file
	 * @param req
	 */
	public void fileUplod(File file, HttpServletRequest req) {

	}

	public List<Map<String, String>> exclImpInfoRead(String impType, String catg_code, File file) {
		int cNum = SystemConfig.getInstance().getInt(""); // 总列数
		int tNum = SystemConfig.getInstance().getInt(""); // 标题行数
		String[] cName = SystemConfig.getInstance().getStringArray(""); // 列对应code

		// 可为空列（int[]{1,2,3}）
		String[] bColumns = SystemConfig.getInstance().getStringArray("");
		int[] blank = new int[bColumns.length];
		for (int i = 0; i < bColumns.length; i++) {
			blank[i] = Integer.valueOf(bColumns[i]);
		}

		// 检验列
		String[] dictTran = SystemConfig.getInstance().getStringArray("");
		int[] dictnum = new int[dictTran.length];
		for (int i = 0; i < dictTran.length; i++) {
			String[] str = dictTran[i].split(":");
			dictnum[i] = Integer.valueOf(str[0]);
			dictTran[i] = str[1];
		}
		List<Map<String, String>> maps = new ArrayList<Map<String, String>>();
		if (bColumns.length > 0) {
			Excels.Reader reader = Excels.newReaderBuilder(cNum).titleRowNum(tNum).blankColumns(blank)
					.dicts(dictnum, dictTran).read(file);
			maps = reader.asMap(cName);
		} else {
			Excels.Reader reader = Excels.newReaderBuilder(cNum).titleRowNum(tNum).dicts(dictnum, dictTran).read(file);
			maps = reader.asMap(cName);
		}

		return maps;

	}

}
