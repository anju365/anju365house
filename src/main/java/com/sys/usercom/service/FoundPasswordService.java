package com.sys.usercom.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.eshop.frame.annotation.Request;
import com.eshop.frame.exception.BusinessException;
import com.eshop.frame.exception.SystemException;
import com.sys.usercom.dao.UserComMapper;
import com.sys.usercom.entity.UserComEntity;
import com.sys.utils.MD5Util;

/**
 * 
 * 密码找回的service
 */
@Service
public class FoundPasswordService {
	@Autowired
	private UserComMapper comMapper;

	// 根据用户的身份证去查询数据库
	public UserComEntity foundPass(@Request("idNumber") String idNumber) {
		if (null == idNumber) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		UserComEntity userComEntity = comMapper.foundPass(idNumber);
		if (null != userComEntity && userComEntity.getUserStatus() == 1) {
			return userComEntity;
		} else {
			throw new BusinessException("USER_CODE_IS_NOT_PASS_AND_NOT_UPDATE");
		}

	}

	// 根据用户的身份证修改密码
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false, rollbackFor = Exception.class)
	public void updatePassword(@Request("userComEntity") UserComEntity userComEntity, HttpServletRequest request) {
		if (null == userComEntity) {
			throw new SystemException("PARAMS_IS_NULL");
		}
		userComEntity.setPassword(MD5Util.MD5(userComEntity.getPassword()));
		try {
			comMapper.updatePassword(userComEntity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
