package com.sys.usercom.service;

import javax.servlet.http.HttpServletRequest;

import com.sys.usercom.entity.UserComEntity;

public interface UserComService {
	UserComEntity getUserByLoginName(String username, char[] password);

	UserComEntity getUserInfo(HttpServletRequest req);
}
