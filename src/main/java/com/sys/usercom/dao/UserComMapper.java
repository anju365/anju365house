package com.sys.usercom.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.eshop.frame.base.dao.BaseMapper;
import com.sys.usercom.entity.UserComEntity;

public interface UserComMapper extends BaseMapper<UserComEntity> {

	/**
	 * 登录名是否存在
	 *
	 * @param userCode
	 *            登录名
	 * @return
	 */
	public Long userIsPro(@Param("userCode") String userCode);

	/**
	 * 登陆用户是否存在
	 * 
	 * @param loginName
	 * @return
	 */
	public UserComEntity checkUserCom(@Param("loginName") String loginName);

	/**
	 * 用户证件号码是否存在
	 *
	 * @param idNumber
	 *            证件号码
	 * @return
	 */
	public Long idNumberIsPro(@Param("idNumber") String idNumber);

	/**
	 * 联系方式是否存在
	 *
	 * @param userTel
	 *            联系方式
	 * @return
	 */
	public Long userTelIsPro(@Param("userTel") String userTel);

	/**
	 * 用户的添加
	 * 
	 * @param userentity
	 */
	public void addUserComEntity(UserComEntity userComEntity);

	/**
	 * 用户的查询
	 *
	 * @param UserComEntity
	 * @return
	 */
	// public List<UserComEntity> getUserComEntitys();

	/**
	 * 用户信息更新
	 *
	 * @param UserComEntity
	 */
	public void updateUserComEntity(List<UserComEntity> UserComEntity);

	/**
	 * 根据用户的身份证号查询数据
	 * 
	 * @param idNumber
	 * @return
	 */
	public UserComEntity foundPass(String idNumber);

	/**
	 * 根据身份证号 更新用户的密码
	 * 
	 * @param idNumber
	 * @param newPassword
	 */
	public void updatePassword(UserComEntity userComEntity);

	/**
	 * 用户信息查询
	 *
	 * @param userComEntity
	 * @return
	 */
	public List<UserComEntity> getUserInfo(UserComEntity userComEntity);

	/**
	 * 用户查询 身份证
	 */
	public UserComEntity findByIdNumber(String idNumber);

}
