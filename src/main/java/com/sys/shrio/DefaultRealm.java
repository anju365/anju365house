package com.sys.shrio;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;

import com.eshop.frame.shiro.AbstractAccountRealm;
import com.sys.usercom.entity.UserComEntity;
import com.sys.usercom.service.UserComService;

public class DefaultRealm extends AbstractAccountRealm {
	@Autowired
	private UserComService userComService;

	@Override
	protected Object getCredentials(Object principal) {
		return ((UserComEntity) principal).getPassword().toCharArray();
	}

	@Override
	protected Object getPrincipal(AuthenticationToken authenticationToken) {
		return userComService.getUserByLoginName(((UsernamePasswordToken) authenticationToken).getUsername(),
				((UsernamePasswordToken) authenticationToken).getPassword());
	}

	@Override
	protected boolean isLock(Object principal) {
		return BooleanUtils.toBoolean(((UserComEntity) principal).getIsLock(), Integer.valueOf(1), Integer.valueOf(0));
	}

}
